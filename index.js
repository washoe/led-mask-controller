const express = require('express');
const fetch = require('node-fetch');
const app = express();
const bodyParser = require('body-parser');
require('dotenv').load({ silent: true }); ;
const port = process.env.PORT;
const accessToken = process.env.ACCESS_TOKEN;
const deviceId = process.env.DEVICE_ID;
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

let maskData = '';

app.listen(port, (err) => {
  return err ?
    console.error(`error starting server: ${err}`) :
    console.log(`server is listening on ${port}`);
});

/**
 * Sends command to mask with hostnameas payload
 * @param {string} data
 */
const pingMask = async (hostname) => {
  const url = `https://api.particle.io/v1/devices/${deviceId}/led?access_token=${accessToken}`;
  const body = {
    arg: hostname
  };
  const result = await fetch(url, {
    method: 'POST',
    body:  JSON.stringify(body),
    headers: {
        'Content-Type': 'application/json'
    }
  });
  console.log('pinged mask', result.status);
  return result;
}


/**
 * POST request is called by web ui
 * Caches data,
 * Pings mask with the url from which data can be fetched
 */
app.post('/mask-data', async (req, res) => {
  console.log(req.body);
  maskData = req.body.data;
  const pingResult = await pingMask(req.hostname);
  res.end('');
})

/**
 * GET request is made by LED Mask
 * Returns cached mask data
 */
app.get('/mask-data', function(req, res) {
  res.send(maskData);
});
