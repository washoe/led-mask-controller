// This #include statement was automatically added by the Particle IDE.
#include <neopixel.h>

// This #include statement was automatically added by the Particle IDE.
#include <HttpClient.h>

// This #include statement was automatically added by the Particle IDE.
#include "lib1.h"
#include "Particle.h"

// not sure what this is for!
SYSTEM_MODE(AUTOMATIC);

/**
* control app is at https://led-mask.herokuapp.com
*/

/**
 * HttpClient setup
 */
HttpClient http;
http_header_t headers[] = {
     { "Content-Type", "text/plain" },
     { "Accept" , "text/plain" },
    { "Accept" , "*/*"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};

http_request_t request;
http_response_t response;

/**
 * Button pin setup
 */

 // IMPORTANT: Set pixel COUNT, PIN and TYPE
#define PIXEL_PIN D7
#define PIXEL_COUNT 140
#define PIXEL_TYPE WS2812B
Adafruit_NeoPixel strip(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);
// LED on the button
int buttonLedPin = D1;
// Our button wired to D0
int buttonPin = D0;

/**
 * Button state constants
 */
int BUTTON_DOWN = 0;
int BUTTON_UP = 1;

/**
 * App state
 */
// current state of the button (0 or 1)
int buttonState;
// received command is stored in cuedData until button is pushed and data is flushed to the LED strip
String cuedData = "";

void setup()
{
   pinMode(buttonPin, INPUT);
   pinMode(buttonLedPin, OUTPUT);

   // When command 'led' is received, run onData function
   Particle.function("led", onCommand);

   // Make sure button LED is off when we start:
   digitalWrite(buttonLedPin, LOW);

     strip.begin();
  strip.show();
}

void loop() {
   updateButtonState();
}

/**
 * Check and store button state.
 * Set button LED state.
 * Perform button action if button has been released
 */
void updateButtonState() {

   // find out if the button is pushed
   // or not by reading from it.
   int newButtonState = digitalRead( buttonPin );
   // If button state has changed...
   if (newButtonState != buttonState)
   {
       // store the new state
       buttonState = newButtonState;
       // Log button state
       Particle.publish(String(buttonState));
       // if the new state is UP...
       if (buttonState == BUTTON_UP)
       {
           // run the button release handler
           onButtonRelease();
       }
   }
   // If there is some data cued, light the button LED
  if( cuedData != "" )
  {
    digitalWrite( buttonLedPin, HIGH);
    // delay(300);
    // digitalWrite( buttonLedPin, LOW);
    // delay(300);
  }else{
    // otherwise
    // turn the LED Off
    digitalWrite( buttonLedPin, LOW);

  }
}

/**
 * Whatever we want to happen when the button is release, we do it here
 */
void onButtonRelease() {
    Particle.publish("Button released");
    // update led strip
    updateLED();
}


/**
 * Update LED strip with cued command, then clear cued command
 */
void updateLED() {
    int numChars = cuedData.length();
    Particle.publish("Pushing command to LED with this many characters", String(numChars));
    // TODO execute command to update LED
    // Particle.publish("cuedData.length()", "BARHGHG");
    uint16_t i;
    for(i=0; i<140; i++) {

        switch(cuedData[i]) {
            case 'r': strip.setPixelColor(i, 50,0,0);
            break;
            case 'g': strip.setPixelColor(i, 0,50,0);
            break;
            case 'b': strip.setPixelColor(i, 0,0,50);
            break;
            case 'c': strip.setPixelColor(i, 0,50,50);
            break;
            case 'm': strip.setPixelColor(i, 50,0,50);
            break;
            case 'y': strip.setPixelColor(i, 50,50,0);
            break;
            case 'w': strip.setPixelColor(i, 50,50,50);
            break;
            case 'k': strip.setPixelColor(i, 0,0,0);
            break;
            // default: strip.setPixelColor(i, 0,0,0);
        }
    }
    strip.show();
    //     Particle.publish("i is", String(i));



    cuedData = "";
}


/**
 * Handle incoming command
 * Command payload is the hostname from which to get the LED data
 */
int onCommand(String command) {
    Particle.publish("command received to get data from host", command);
    getData(command);
    return 1;
}

/**
 * Get led data from out express server at the given hostname
 * Store it in cuedCommand
 */
void getData(String hostname) {
    request.hostname = hostname;
    request.port = 80;
    request.path = "/mask-data";
    http.get(request, response, headers);
    // store cued command
    cuedData = response.body;
    Particle.publish("Mask data: ", cuedData);
}
