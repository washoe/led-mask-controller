## Web-controlled LED Mask
This is a project I've been working on with my son. He'd seen some [videos on YouTube](https://www.youtube.com/watch?v=KEPldnI0fgk) showing a face mask sporting RGB LEDs controlled by the [Particle Photon](https://store.particle.io/products/photon).

We decided that we would go one better and create a mask that could be controlled remotely over the net.

This is still very much a work in progress, with a few hardware and software challenges yet to overcome.

Our mask comprises 140 WS2812 addressable LEDs, arranged in a triangle shape to cover the lower part of the face. There is a push button for pushing cached data to the mask. An LED on the button indicates when the data has been updated, allowing the user to control the timing of the update.

### Architecture

* Front end single page app. Built using Vuejs, this app allows the user to select from predefined lighting configurations and push them to the mask.
* Express middleware. Mediates between the front end and the mask itself. This is required to get around some of the limitations of the Particle cloud api, as well as to preserve the secrecy of the device id and access token.
* Particle Photon firmware. Written in C++, it communicates with our middleware to receive commands and data.
